const BASE_URL = "https://noroff-komputer-store-api.herokuapp.com/"

//DOM-ELEMENTS
let bankBalanceElement = document.getElementById("bankBalance");
let loanBalanceTitleElement = document.getElementById("loanBalanceTitle");
let loanBalanceElement = document.getElementById("loanBalance");
let salaryBalanceElement = document.getElementById("salaryBalance");
const laptopsElement = document.getElementById("laptopSelection");
const laptopFeaturesElement = document.getElementById("laptopFeatures");

//DOM-ELEMENST---LAPTOP INFORMATION AREA
const laptopImageElement = document.getElementById("laptopImage");
const laptopTitleElement = document.getElementById("laptopTitle");
const laptopDescriptionElement = document.getElementById("laptopDescription");
const laptopPriceElement = document.getElementById("laptopPrice");

//BUTTONS
const getLoanBtn = document.getElementById("getLoanBtn");
const bankInSalaryBtn = document.getElementById("bankInSalaryBtn");
const workBtn = document.getElementById("workBtn");
const repayLoanBtn = document.getElementById("repayLoanBtn");
const buyLaptopBtn = document.getElementById("buyLaptopBtn");

//-----------------------------------------------------------
//Work and Bank properties
let bankBalance = 15000; //default value in bank
let salaryBalance = 0;
let countLoan = 0;
let outstandingLoan = 0;
let formatNOK = new Intl.NumberFormat('no-NO', {style: 'currency', currency: 'NOK'});
//Laptop Properties
let laptops = [];

//-----------------------------------------------------------
fetch(BASE_URL + "computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToSelection(laptops));

function addLaptopsToSelection(laptops){
    laptops.forEach(x => addLaptopToSelection(x));
    const laptop = laptops[0].specs;

    let text = "";
    for(i = 0; i < laptop.length; i++) {
        text += laptop[i] + "\n";
    }

    //Display the first laptop as default 
    laptopFeaturesElement.innerText = text;

    laptopImageElement.innerHTML = `<img src="${BASE_URL + laptops[0].image}" width=200"/>`;
    laptopTitleElement.innerText = laptops[0].title;
    laptopDescriptionElement.innerText =laptops[0].description;
    laptopPriceElement.innerText = `${laptops[0].price} kr`;
}

function addLaptopToSelection(laptop){
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

function handleLaptopChange(e){
    const selectedLaptop = laptops[e.target.selectedIndex];
    let featuresArray = selectedLaptop.specs;

    let text = "";
    for(i = 0; i < featuresArray.length; i++) {
        text += featuresArray[i] + "\n";
    }
    laptopFeaturesElement.innerText = text;

    //Laptop Information
    laptopImageElement.innerHTML = `<img src="${BASE_URL + selectedLaptop.image}" width=200"/>`;
    laptopTitleElement.innerText = selectedLaptop.title;
    laptopDescriptionElement.innerText =selectedLaptop.description;
    laptopPriceElement.innerText = `${selectedLaptop.price} kr`;

}


renderBalance();

//-----------------------------------------------------------
//EVENT LISTENERS
workBtn.addEventListener("click", work);
bankInSalaryBtn.addEventListener("click", bankInSalary);
getLoanBtn.addEventListener("click", getLoan);
laptopsElement.addEventListener("change", handleLaptopChange);
repayLoanBtn.addEventListener("click", repayLoan);
buyLaptopBtn.addEventListener("click", buyLaptop);

//-----------------------------------------------------------
//FUNCTIONS

function renderBalance(){ //Render the balances that are displayed
    bankBalanceElement.innerText = formatNOK.format(bankBalance);
    salaryBalanceElement.innerText = formatNOK.format(salaryBalance);
    console.log("the bankBalance and salaryBalance has been rendered")
    showLoanBalance();
}

//-----BANK-----
function getLoan(){
    let userInputLoan = prompt("Please enter the amount you want to loan");

    //Loan constraints
    if(checkHasLoan()){
        alert("You need to repay the loan you already have!");
    }
    if(userInputLoan > (2*bankBalance)){//check if userInputLoan is greater than the double of bankBalance
        alert("You can't get a loan higher than "+formatNOK.format(2*bankBalance)+"!");
    }
    if(userInputLoan == null || userInputLoan == ""){//alert if empty input
        alert("Please enter an amount you want to loan");
    }
    else if(!checkHasLoan() && userInputLoan <= (2*bankBalance)){
        countLoan += 1;
        outstandingLoan = userInputLoan;   
        renderBalance();
    }
}

function showLoanBalance(){ //show the loanBalance if user has loan else it's hidden
    if(checkHasLoan()){
        loanBalanceTitleElement.style.display = "block"; 
        loanBalanceElement.style.display = "block";
        loanBalanceElement.innerText = formatNOK.format(outstandingLoan);
        repayLoanBtn.style.display="block";
        console.log("showLoanBalance initialized");
    }
    else{
        loanBalanceTitleElement.style.display = "none";
        loanBalanceElement.style.display = "none";
        repayLoanBtn.style.display="none";
    }
}

function checkHasLoan(){ //check if user has a loan
    if(countLoan > 0){
        return true;
        console.log("has outstandingLoan");
    }   
    else{
        return false;
    }
}

//-----WORK--------
function bankInSalary(){ //bank in the salary
    if(checkHasLoan()){
        //if user has loan, deduce 10% of salary to pay loan
        outstandingLoan = outstandingLoan - (0.1 * salaryBalance);
        salaryBalance = salaryBalance - (0.1 * salaryBalance);
        bankBalance += salaryBalance;
        salaryBalance = 0;
    }else{
        bankBalance += salaryBalance;
        salaryBalance = 0;
    }
    console.log("bankInSalary function initialized")
    renderBalance();
}

function work(){ //increases the salaryBalance by 100
    salaryBalance += 100; 
    console.log(salaryBalance, "work function initialized");
    renderBalance();
}

function repayLoan(){ 
    if(checkHasLoan()){
        if(salaryBalance < outstandingLoan){
            outstandingLoan = outstandingLoan - salaryBalance;
            salaryBalance = 0;
        }
        else{
            bankBalance += (salaryBalance - outstandingLoan);
            outstandingLoan = 0;
            salaryBalance = 0;
            countLoan = 0;
        }
    }
    renderBalance(); 
}


function buyLaptop(){
    const laptopValue = laptopPriceElement.innerText;
    const laptopPrice = parseInt(laptopValue);
    const laptopTitle = laptopTitleElement.innerText;
    if(bankBalance < laptopPrice){
        alert("You don't have sufficient money to buy the laptop!");
    }
    else{
        bankBalance = bankBalance - laptopPrice;
        alert("Hoorayyy! You are now the owner of "+laptopTitle+ "!");
    }
    renderBalance();
}


